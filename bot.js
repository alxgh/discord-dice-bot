const Discord = require('discord.js');
const logger = require('winston');
require('dotenv').config()

parse = (content) => {
  let text = '', count, dice, modifier, type, numbers = [], chosen;
  while((r = regex.exec(content)) !== null) {
    count = parseInt(r[1]) || 1;
    dice = parseInt(r[2]);
    modifier = parseInt(r[3]) || 0;
    type = r[4] || 's'
    number = 0;
    numbers = [];
    text += `Rolling ${count} d${dice}(s) with ${modifier} modifier:\n`;
    for(let i = 0; i < count; i++) {
      n = Math.floor((Math.random() * dice) + 1);
      if(n < 1) n = 1;
      text += `${n}   `;
      numbers.push(n);
    }

    if(type == 'a') {
      chosen = Math.max(...numbers);
    }
    if(type == 'd') {
      chosen = Math.min(...numbers);
    }
    if(type == 's') {
      chosen = numbers.reduce((total, t) => total += t);
      
    }
    chosen += modifier;
    if(chosen == dice && (numbers.length == 1 || type !== 's')) chosen += ' :partying_face:';
    text += ` -> ${chosen}  \n`
  }
  return text;
}

// Configure logger settings
logger.remove(logger.transports.Console);
logger.add(new logger.transports.Console, {
    colorize: true
});
logger.level = 'debug';
// Initialize Discord Bot
var bot = new Discord.Client();
let regex = /([0-9]+)?[d]([0-9]+)([+-][0-9]+)?([ad])?/g

bot.on('ready', function (evt) {
    logger.info('Connected');
    logger.info('Logged in as: ');
    logger.info(bot.username + ' - (' + bot.id + ')');
});
bot.on('message', msg => {
    if (msg.content.slice(0,5) === '!roll') {
      let text = parse(msg.content);
      msg.reply(text);
    }
  });
bot.login(process.env.DISCORD_TOKEN);